package App::PtLink::CachableInput;

use strict;
use warnings;

use Moose::Role;
use MLDBM qw/DB_File Storable/;
use App::PtLink::GlobalOptions qw/global_option/;
use DateTime::Format::HTTP;
use DateTime::Duration;
use DateTime;
use Mojo::URL;
use Fcntl;

my $inited = 0;
my %db;

sub _build_raw {
	if(!$inited) {
		## no critic (ProhibitLeadingZeros);
		tie %db, 'MLDBM', global_option('cachedir') . "/ptlink-cache.dbm", O_CREAT|O_RDWR, 0640 or die $!;
		## use critic
		$inited = 1;
	}
	my $self = shift;
	my $o = {data => undef};
	my $url = $self->url->to_string;
	if(exists($db{$url})) {
		$o = $db{$url};
		if(exists($o->{redirect_to})) {
			$self->url(Mojo::URL->new($o->{redirect_to}));
			return $self->_build_raw;
		}
		if($self->skip_online || (exists($o->{expirydate}) && $o->{expirydate} > DateTime->now())) {
			return $o->{data};
		}
		if(exists($o->{last_change})) {
			$self->last_change($o->{last_change});
		}
	}
	my $rv;
	eval {$rv = $self->_raw_uncached};
	if(!defined($rv)) {
		if(exists($o->{data}) && defined($o->{data})) {
			return $o->{data};
		}
		die $@
	}
	return $rv;
}

sub DESTROY {
	my $self = shift;
	if($self->has_raw && ($self->has_last_change || $self->has_expirydate)) {
		my $o = {};
		$o->{data} = $self->raw;
		if($self->original_url ne $self->url && $self->should_cache_redirect) {
			my $r = {redirect_to => $self->url->to_string};
			$db{$self->original_url->to_string} = $r;
		}
		if($self->has_last_change) {
			$o->{last_change} = $self->last_change;
		}
		if($self->has_expirydate) {
			$o->{expirydate} = $self->expirydate;
		}
		$db{$self->url->to_string} = $o;
	}
}

sub _has_cache_for {
	return exists($db{$_[1]});
}

1;
