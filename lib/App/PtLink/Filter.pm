package App::PtLink::Filter;

use Moose::Role;
use Carp;

use App::PtLink::FormattedText;

requires "filtered_text";

has "text" => (
	is => "rw",
	isa => "App::PtLink::FormattedText",
	predicate => "has_text",
);

has "next" => (
	is => "rw",
	isa => "App::PtLink::Filter",
	predicate => "has_next",
);

sub _get_unfiltered {
	my $self = shift;
	if($self->has_next) {
		return $self->next->filtered_text;
	} elsif($self->has_text) {
		return $self->text;
	} else {
		my $type = $self->class_name;
		croak "can't get unfiltered message for $type: neither a message nor a next filter were provided";
	}
}

1;
