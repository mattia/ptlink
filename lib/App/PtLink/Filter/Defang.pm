package App::PtLink::Filter::Defang;

use Moose;

use HTML::Defang;

use App::PtLink::FormattedText;

extends "App::PtLink::Base";

with "App::PtLink::Filter";

my $defang = HTML::Defang->new(
	delete_defang_content => 1,
	fix_mismatched_tags => 1,
);

sub filtered_text {
	my $self = shift;
	my $text = $self->_get_unfiltered;
	if($text->mimetype ne "text/html") {
		die "Cannot defang HTML: message is not in HTML format";
	}
	return App::PtLink::FormattedText->new(text => $defang->defang($text->text), mimetype => "text/html");
}

no Moose;

1;
