package App::PtLink::Filter::Stdio;

use Moose;

use App::PtLink::FormattedText;
use IPC::Open2;

extends "App::PtLink::Base";

with "App::PtLink::Filter";

has "command" => (
	is => "rw",
	required => 1,
	isa => "ArrayRef[Str]",
);

has "alters_mime" => (
	is => "rw",
	default => 0,
	isa => "Bool",
);

sub filtered_text {
	my $self = shift;
	my $text = $self->_get_unfiltered;
	my $command = $self->command;
	push @$command, $text->mimetype;
	my $pid = open2(my $filtered, my $unfiltered, @$command);
	binmode $unfiltered, ":encoding(utf8)";
	binmode $filtered, ":encoding(utf8)";
	print $unfiltered $text->text;
	close($unfiltered);
	my $new_type = $text->mimetype;
	if($self->alters_mime) {
		$new_type = <$filtered>;
	}
	local $/;
	waitpid($pid, 0);
	die "failed to run filter: waitpid returned $?" unless $? >> 8 == 0;
	my $new_text = <$filtered>;
	return App::PtLink::FormattedText->new(mimetype => $new_type, text => $new_text);
}

no Moose;

1;
