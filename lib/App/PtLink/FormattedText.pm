package App::PtLink::FormattedText;

use Moose;

extends 'App::PtLink::Base';

has 'text' => (
	is => 'rw',
	required => 1,
	isa => 'Str',
);

has 'mimetype' => (
	is => 'rw',
	required => 1,
	isa => 'Str',
);

sub as {
	my $self = shift;
	my $wanted = shift;
	if($self->mimetype ne $wanted) {
		...
	}
	return $self->text;
}

no Moose;
1;
