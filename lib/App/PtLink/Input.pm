package App::PtLink::Input;

use App::PtLink;
use App::PtLink::Base;
use App::PtLink::Message;
use App::PtLink::Metadata;
use Moose;
use Moose::Util::TypeConstraints;
use Mojo::URL;
use Mojo::UserAgent;
use DateTime::Format::HTTP;

my $ua = Mojo::UserAgent->new;
$ua->proxy->detect;
$ua->transactor->name('PtLink/' . $App::PtLink::VERSION);

extends 'App::PtLink::Base';

coerce "Mojo::URL",
	from "Str",
	via { Mojo::URL->new($_) };

has 'url' => (
	is => 'rw',
	isa => 'Mojo::URL',
	required => 1,
	coerce => 1,
	trigger => sub { shift->_clear_all_raw },
);

sub _clear_all_raw {
	my $self = shift;
	$self->clear_raw;
	$self->_clear_raw_uncached;
	$self->clear_last_change;
}

has 'original_url' => (
	is => 'ro',
	init_arg => "url", # hack: use the same argument as for the URL
);

has 'messages' => (
	is => 'ro',
	isa => 'ArrayRef[App::PtLink::Message]',
	lazy_build => 1,
);


has 'dom' => (
	is => 'rw',
	isa => 'Mojo::DOM',
	lazy_build => 1,
);

sub _build_dom {
	return Mojo::DOM->new(shift->raw);
}

has 'max_messages' => (
	is => 'rw',
	isa => 'Int',
	default => -1,
);

has 'raw' => (
	is => 'ro',
	isa => 'Str',
	lazy_build => 1,
);

sub _build_raw {
	return shift->_raw_uncached;
}

has '_raw_uncached' => (
	is => 'ro',
	isa => 'Str',
	lazy_build => 1,
);

has 'skip_online' => (
	is => 'rw',
	isa => 'Bool',
	default => 0,
);

sub _build__raw_uncached {
	my $self = shift;
	die "fetching data disabled" if $self->skip_online;

	my $res;
	if($self->url->scheme eq "file") {
		my $fn = $self->url->host . $self->url->path;
		my @stat = stat($fn) or return;
		my $mtime = DateTime->from_epoch($stat[9]);
		return if($self->has_last_change && $self->last_change <= $mtime);
		open my $fh, "<:encoding(utf-8)", $fn or return;
		local $/ = undef;
		my $body = "";
		while(<$fh>) {
			$body .= $_;
		}
		$self->last_change($mtime);
		close $fh;
		return $body;
	}
	if(!$self->has_last_change) {
		$res = $ua->get($self->url)->result;
		$self->dom($res->dom);
	} else {
		$res = $ua->get($self->url => {"If-Modified-Since" => DateTime::Format::HTTP->format_datetime($self->last_change)})->result;
	}
	if($res->code == 302 || $res->code == 307) {
		$self->should_cache_redirect(0);
	}
	if(defined($res->headers->location)) {
		$self->url(Mojo::URL->new($res->headers->location)->base($self->url)->to_abs);
		return $self->_build__raw_uncached;
	}
	die "Could not access " . $self->url . ": " . $res->code . " " . $res->message . "\n" unless $res->is_success;
	if(defined($res->headers->last_modified)) {
		$self->last_change(DateTime::Format::HTTP->parse_datetime($res->headers->last_modified));
	}
	if(defined($res->headers->expires)) {
		eval {
			$self->expirydate(DateTime::Format::HTTP->parse_datetime($res->headers->expires));
		};
		if($@) {
			# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expires
			# says:
			#
			# "Invalid expiration dates with value 0 represent a
			# date in the past and mean that the resource is
			# already expired."
			#
			# Set it to now, which is approximately the same thing.
			$self->expirydate(DateTime->now);
		}
	}
	return $res->body;
}

has 'metadata' => (
	is => 'ro',
	isa => 'App::PtLink::Metadata',
	lazy_build => 1,
);

has 'last_change' => (
	is => 'rw',
	isa => 'DateTime',
	predicate => 'has_last_change',
	clearer => 'clear_last_change',
);

has 'expirydate' => (
	is => 'rw',
	isa => 'DateTime',
	predicate => 'has_expirydate',
);

has 'should_cache_redirect' => (
	is => 'rw',
	isa => 'Bool',
	default => 1,
	lazy => 1,
);

has 'input_type' => (
	is => 'rw',
	isa => 'Str',
	lazy_build => 1,
);


sub create {
	my $class = shift;
	my $format = shift;
	my $options = \@_ ;

	eval "require App::PtLink::Input::$format;" or die $!; ## no critic(StringyEval)
	my $rv = "App::PtLink::Input::$format"->new(@$options);
	return $rv;
}

no Moose;
1;
