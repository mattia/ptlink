package App::PtLink::Input::XmlFeed;

use XML::Feed;
use Moose;

use App::PtLink::Util::NoUndefHash;
use App::PtLink::FormattedText;
use App::PtLink::Message;
use App::PtLink::Author;
use Email::Address;
use File::Type;

my $ft = File::Type->new();

extends "App::PtLink::Input";

has 'xmlfeed' => (
	is => 'rw',
	isa => 'XML::Feed',
	lazy_build => 1,
);

sub _clear_all_raw {
	my $self = shift;
	$self->clear_xmlfeed;
	$self->SUPER::_clear_all_raw;
}

sub _build_input_type {
	return "xmlfeed";
}

sub _build_xmlfeed {
	my $self = shift;
	my $data = $self->raw;
	my $base = $self->dom->at("head base");
	if($ft->checktype_contents($data) eq "text/html") {
		if(defined($base)) {
			$base = Mojo::URL->new($base->{href});
		} else {
			$base = $self->url;
		}
		foreach my $link($self->dom->find('head link')->each) {
			if(exists($link->{rel})
					&& $link->{rel} eq "alternate"
					&& exists($link->{type})
					&& grep { $link->{type} eq $_ }
						qw|application/rss+xml application/atom+xml application/rdf+xml application/xml text/xml|) {
				$self->url(Mojo::URL->new($link->{href})->base($base)->to_abs);
				return $self->_build_xmlfeed;
			}
		}
		die "The content at " . $self->url . " contains HTML with no links to an XML feed. I can't parse this.\n"
	}
	my $feed = XML::Feed->parse(\$data) or die "XML::Feed failed to parse: " . XML::Feed->errstr . "\n";
	return $feed;
}

sub _build_metadata {
	my $self = shift;
	tie my %args, 'App::PtLink::Util::NoUndefHash';
	$args{title} = $self->xmlfeed->title;
	$args{link} = $self->xmlfeed->link;
	$args{description} = $self->xmlfeed->description;
	$args{authors} = [ map{
		tie my %aargs, 'App::PtLink::Util::NoUndefHash';
		$aargs{name} = $_->name;
		$aargs{nick} = $_->user;
		$aargs{email} = $_->address;
		App::PtLink::Author->new(%aargs) } Email::Address->parse($self->xmlfeed->author) ];
	if(!exists($args{authors}) && defined($self->xmlfeed->author) && length($self->xmlfeed->author) > 0) {
		$args{authors} = [ App::PtLink::Author->new(name => $self->xmlfeed->author) ];
	}
	return App::PtLink::Metadata->new(%args);
}

sub _build_messages {
	my $self = shift;
	my $rv = [];
	foreach my $entry ($self->xmlfeed->entries) {
		last if($self->max_messages > 0 && (scalar(@$rv)) >= $self->max_messages);
		tie my %margs, 'App::PtLink::Util::NoUndefHash';
		my %fargs = (
			text => (defined($entry->content->body) ? $entry->content->body : $entry->title),
			mimetype => (defined($entry->content->type) ? $entry->content->type : 'text/html'),
		);
		next unless defined($fargs{text});
		$margs{body} = App::PtLink::FormattedText->new(%fargs);
		$margs{title} = $entry->title;
		$margs{url} = Mojo::URL->new($entry->link);
		$margs{authors} = [ map {
			tie my %aargs, 'App::PtLink::Util::NoUndefHash';
			$aargs{name} = $entry->name;
			$aargs{nick} = $entry->user;
			$aargs{email} = $entry->address;
			App::PtLink::Author->new(%aargs) } Email::Address->parse($entry->author)];
		if(!exists($margs{authors}) && defined($self->xmlfeed->author) && length($self->xmlfeed->author) > 0) {
			$margs{authors} = [ App::PtLink::Author->new(name => $self->xmlfeed->author) ];
		}
		$margs{timestamp} = $entry->issued;
		$margs{guid} = $entry->id;
		$margs{source} = $self;
		push @$rv, App::PtLink::Message->new(%margs);
	}
	return $rv;
}

no Moose;

1;
