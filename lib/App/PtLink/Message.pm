package App::PtLink::Message;

use Moose;

extends 'App::PtLink::Base';

has 'title' => (
	is => 'rw',
	isa => 'Str',
	predicate => 'has_title',
);

has 'body' => (
	is => 'rw',
	isa => 'App::PtLink::FormattedText',
	predicate => 'has_body',
);

has 'authors' => (
	is => 'rw',
	isa => 'ArrayRef[App::PtLink::Author]',
	predicate => 'has_authors',
);

has 'language' => (
	is => 'rw',
	isa => 'Maybe[Str]',
	lazy_build => 1,
);

sub _build_language {
	my $self = shift;
	if($self->has_source && defined($self->source)) {
		return $self->source->metadata->language;
	}
	return;
}

has 'timestamp' => (
	is => 'rw',
	isa => 'DateTime',
	predicate => 'has_timestamp',
);

has 'url' => (
	is => 'rw',
	isa => 'Mojo::URL',
	predicate => 'has_url',
);

has 'guid' => (
	is => 'rw',
	isa => 'Str',
	predicate => 'has_guid',
);

has 'source' => (
	is => 'ro',
	isa => 'App::PtLink::Input',
	weak_ref => 1,
	predicate => 'has_source',
);

sub all_attributes {
	my $self = shift;
	if($self->has_source && $self->source->has_attributes && $self->has_attributes) {
		return { %{$self->source->attributes}, %{$self->attributes} };
	} elsif($self->has_source && $self->source->has_attributes) {
		return $self->source->attributes;
	} elsif($self->has_attributes) {
		return $self->attributes;
	} else {
		return;
	}
}

no Moose;
1;
