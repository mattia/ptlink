package App::PtLink::Metadata;

use Moose;

has 'title' => (
	is => 'ro',
	isa => 'Str',
);

has 'link' => (
	is => 'ro',
	isa => 'Str',
);

has 'language' => (
	is => 'ro',
	isa => 'Str',
);

has 'description' => (
	is => 'ro',
	isa => 'Maybe[Str]',
);

has 'default_authors' => (
	is => 'ro',
	isa => 'ArrayRef[App::PtLink::Author]',
	predicate => 'has_default_authors',
);

no Moose;
1;
