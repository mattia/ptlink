package App::PtLink::Output;

use App::PtLink::Base;
use App::PtLink::Message;
use App::PtLink::Metadata;
use Moose;
use Mojo::URL;
use Mojo::UserAgent;
use Carp;

extends 'App::PtLink::Base';

has 'url' => (
	is => 'rw',
	isa => 'Mojo::URL',
);

has 'messages' => (
	is => 'rw',
	isa => 'ArrayRef[App::PtLink::Message]',
	traits => ['Array'],
	default => sub { [] },
	handles => {
		add_messages => 'push',
		map_messages => 'map',
		sorted_messages => 'sort',
		filtered_messages => 'grep',
	}
);

has 'relevant_messages' => (
	is => 'ro',
	isa => 'ArrayRef[App::PtLink::Message]',
	lazy_build => 1,
);

sub _build_relevant_messages {
	my $self = shift;
	return $self->messages;
}

has 'metadata' => (
	is => 'rw',
	isa => 'App::PtLink::Metadata',
	lazy_build => 1,
);

has 'title' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_title',
);

has 'description' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_description',
);

has 'language' => (
	is => 'ro',
	isa => 'Str',
	predicate => 'has_language',
);

sub create {
	my $class = shift;
	my $format = shift;
	my $options = [ @_ ];

	eval "require App::PtLink::Output::$format;" or die $!; ## no critic(StringyEval)
	my $rv = "App::PtLink::Output::$format"->new(@$options);
	return $rv;
}

sub commit_messages {
	...
}

no Moose;
1;
