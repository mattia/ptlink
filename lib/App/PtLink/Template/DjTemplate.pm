package App::PtLink::Template::DjTemplate;

use Moose;

use Dotiac::DTL;

extends 'App::PtLink::Template';

sub handle {
	my $self = shift;
	my $output = shift;
	my $template = Dotiac::DTL->new($self->template_file);
	my $config = $output->attributes;
	return $template->string( %{ $self->params }, Config => $config );
}
