package App::PtLink::Template::HtmlTemplate;

use strict;
use warnings;

use Moose;
use HTML::Template;

extends 'App::PtLink::Template';

sub drop_comments {
	my $text = shift;
	$$text =~ s/#.*$//gm;
	$text;
}

sub handle {
	my $self = shift;
	my $output = shift;
	my $template = HTML::Template->new(filename => $self->template_file, die_on_bad_params => 0, filter => \&drop_comments);
	$template->param(%{$self->params});
	return $template->output;
}

no Moose;

1;
