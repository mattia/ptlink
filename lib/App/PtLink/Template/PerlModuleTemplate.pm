package App::PtLink::Template::PerlModuleTemplate;

use Moose;
use App::PtLink;

extends "App::PtLink::Template";

sub handle {
	my $self = shift;
	my $output = shift;

	my $name = do($self->template_file);
	my $template = bless {%$self}, $name;
	return $template->handle($output);
}
