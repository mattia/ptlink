#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Mojo::URL;

use_ok('App::PtLink::Input');
use_ok('App::PtLink::Input::XmlFeed');

my $url = Mojo::URL->new('file://./t/test-files/ikiwiki-grep.atom');

my $input = App::PtLink::Input->new(url => $url);

isa_ok($input, "App::PtLink::Input");

my $atom = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($atom, "App::PtLink::Input");

my $messages = $atom->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 10, "The number of messages is parsed correctly");
isa_ok($messages->[0], "App::PtLink::Message");
my $first_msg = $messages->[0];
isa_ok($first_msg->authors, "ARRAY");
ok($first_msg->authors->[0]->name eq "WEBlog -- Wouter's Eclectic Blog", "The name of the author was parsed correctly");

done_testing;
