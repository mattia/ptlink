#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;

use_ok("App::PtLink::Base");

my $base = App::PtLink::Base->new;

ok(defined($base), "we can create a base object");
ok($base->num_attributes == 0, "we don't create any attributes by default");

done_testing
