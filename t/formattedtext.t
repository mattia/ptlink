#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;

use_ok("App::PtLink::FormattedText");

my $text = App::PtLink::FormattedText->new(text => "test", mimetype => "text/plain");

ok($text->as("text/plain") eq "test", "non-conversion returns the original text, unmodified");
TODO: {
	local $TODO = "conversion not yet implemented";
	eval {ok($text->as("text/html") eq "<p>test</p>", "conversion succeeds")};
	ok(!$@, "conversion doesn't throw an error");
}

done_testing;
