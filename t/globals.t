#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;

use_ok("App::PtLink::GlobalOptions", qw/global_option set_global_option/);
ok(global_option("cachedir") eq "/tmp", "the cachedir option is read correctly");
set_global_option(cachedir => "/foo");
ok(global_option("cachedir") eq "/foo", "setting options works");
eval {
	global_option("doesnotexist");
};
ok($@ =~ /unknown option/, "unknown options throw");

done_testing;
