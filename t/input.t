#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;

use Moose::Util qw/apply_all_roles/;

use_ok("App::PtLink::Input");
use_ok("App::PtLink::CachableInput");
use_ok("App::PtLink::GlobalOptions", qw/set_global_option/);

set_global_option("cachedir" => "./t");

my $url = Mojo::URL->new("file://./t/test-files/ikiwiki-grep-rss.xml");

my $input = App::PtLink::Input->new(url => $url, skip_online => 1);

isa_ok($input, "App::PtLink::Input");

eval {
	$input->messages;
};
ok($@ =~ /does not support.*_build_messages/, "we can't parse messages on a bare Input object");

$input = App::PtLink::Input->create("XmlFeed", url => $url, skip_online => 1);
eval {
	$input->messages;
};
ok($@ =~ /fetching data disabled/, "fetching data doesn't work when disabled");

eval {
	my $fail = App::PtLink::Input->create("doesnotexist");
};
ok($@ =~ /No such file or directory/, "creating an input of unknown format throws");

my $input2 = App::PtLink::Input->create("XmlFeed", url => $url);
apply_all_roles($input2, "App::PtLink::CachableInput");
$input2->messages;

$input2 = undef;

apply_all_roles($input, "App::PtLink::CachableInput");

my $msgs = $input->messages;
ok(scalar(@$msgs) > 0, "we can reach cached data, even when messages are not available");

unlink("./t/ptlink-cache.dbm");

done_testing
