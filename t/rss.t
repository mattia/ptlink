#!/usr/bin/perl -w

use strict;
use warnings;

use Test::More;
use Mojo::URL;

use_ok('App::PtLink::Input');
use_ok('App::PtLink::Input::XmlFeed');

my $url = Mojo::URL->new("file://./t/test-files/ikiwiki-grep-rss.xml");

my $input = App::PtLink::Input->new(url => $url);

isa_ok($input, "App::PtLink::Input");

my $rss = App::PtLink::Input->create("XmlFeed", url => $url);

isa_ok($rss, "App::PtLink::Input");

my $messages = $rss->messages;

isa_ok($messages, "ARRAY");
ok(scalar(@$messages) == 10, "The number of messages is parsed correctly");
isa_ok($messages->[0], "App::PtLink::Message");
my $first_msg = $messages->[0];
ok(!defined($first_msg->authors), "The first post has no authors");
ok($first_msg->guid eq "https://grep.be/blog//en/computer/Different_types_of_Backups/", "The first message has the correct GUID");
ok($first_msg->title eq "Different types of Backups", "The first message's title is parsed correctly");
my $last_msg = $messages->[9];
isa_ok($last_msg, "App::PtLink::Message");
ok($last_msg->title eq "On Statements, Facts, Hypotheses, Science, Religion, and Opinions", "The last message's title is parsed correctly");

ok(!defined($last_msg->language), "the language is empty");

$rss = undef;
ok(!defined($first_msg->language), "also here, but without source");

done_testing;
