#!/usr/bin/perl -w

use v5.28;

use Test::More;

use_ok("App::PtLink::Input");
use_ok("App::PtLink::Input::XmlFeed");

my $url = Mojo::URL->new("file://./t/test-files/test.html");
my $input = App::PtLink::Input->create("XmlFeed", url => $url);
isa_ok($input, "App::PtLink::Input");
isa_ok($input, "App::PtLink::Input::XmlFeed");
ok($input->input_type eq "xmlfeed", "The type attribute comes back correctly");
my $msgs = $input->messages;
ok(scalar(@$msgs) > 0, "xmlfeed finds stuff through an HTML page");

my $meta = $input->metadata;

ok($meta->title eq "WEBlog -- Wouter's Eclectic Blog", "the title metadata attribute gets parsed correctly");

$url = Mojo::URL->new("file://./t/test-files/test-base.html");

$input->url($url);
ok(!$input->has_raw, "changing the URL clears the raw data");
ok(!$input->_has_raw_uncached, "changing the URL clears the uncached data");
ok(!$input->has_xmlfeed, "changing the URL clears the XML::Feed data");
$input = App::PtLink::Input->create("XmlFeed", url => $url);
my $msgs2 = $input->messages;
$input = undef;
is_deeply($msgs2, $msgs, "finding the XML feed through a document with a base tag returns the correct data");

$input = App::PtLink::Input->create("XmlFeed", url => $url);
$input->max_messages(1);
ok(scalar(@{$input->messages}) == 1, "setting a max_messages value returns no more than the requested amount");

$input = App::PtLink::Input->create("XmlFeed", url => Mojo::URL->new("file://./t/test-files/test-blank.html"));

eval {
	$input->messages;
};
ok($@ =~ /contains HTML with no links/, "giving xmlfeed something that doesn't contain a feed throws");

$input = App::PtLink::Input->create("XmlFeed", url => Mojo::URL->new("file://./t/test-files/notafeed.xml"));

eval {
	$input->messages;
};
ok($@ =~ /XML::Feed failed to parse/, "something that isn't a feed gets rejected by XML::Feed");
done_testing;
