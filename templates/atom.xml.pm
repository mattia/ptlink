## no critic(RequireFilenameMatchesPackage)
package App::PtLink::Template::AtomTemplate;

use Moose;
use XML::Feed;
use App::PtLink;

extends "App::PtLink::Template";

sub handle {
	my $self = shift;
	my $output = shift;
	my $feed = XML::Feed->new("Atom");
	if($output->has_title) {
		$feed->title($output->title);
	}
	if($output->has_description) {
		$feed->description($output->description);
	}
	if($output->has_language) {
		$feed->language($output->language);
	}
	$feed->generator("PtLink/" . $App::PtLink::VERSION);
	foreach my $message(@{$output->relevant_messages}) {
		my $entry = XML::Feed::Entry->new("Atom");
		if($message->has_title) {
			$entry->title($message->title);
		}
		if($message->has_url) {
			$entry->link($message->url->to_string);
		}
		if($message->has_body) {
			$entry->content($message->body->as('text/html'));
		}
		if($message->has_authors) {
			$entry->author($message->authors->[0]->string);
		}
		if($message->has_timestamp) {
			$entry->modified($message->timestamp);
		}
		$feed->add_entry($entry);
	}
	return $feed->as_xml;
}

package main;

"App::PtLink::Template::AtomTemplate";
